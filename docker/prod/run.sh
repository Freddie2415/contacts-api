#!/bin/sh

#cd /var/www
composer install
php artisan storage:link
php artisan migrate --seed --force
php artisan scribe:generate
php artisan cache:clear
php artisan route:cache

/usr/bin/supervisord -c /etc/supervisord.conf

<?php

namespace Database\Seeders;

use App\Models\Ayat;
use App\Models\Juz;
use App\Models\Surah;
use App\Models\Version;
use Illuminate\Database\Seeder;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        $version = Version::first();
        if ($version == null) {
            Version::create(['version' => 1]);
        }

        if (Ayat::count() <= 0) {
            Surah::factory(104)->create();
            Juz::factory(30)->create();
            $juzs = Juz::all();
            Surah::all()
                ->each(function (Surah $surah) use ($juzs) {
                    $count = fake()->numberBetween(1, 100);
                    $juz = $juzs->random(1)->first();
                    Ayat::factory()->count($count)->create(['surah_id' => $surah->id, 'juz_id' => $juz->id]);
                });
        }
    }
}

<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Juz>
 */
class JuzFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'title' => fake('ru_RU')->realText(20),
            'latTitle' => fake('en_US')->realText(20),
            'arabTitle' => fake('ar_SA')->realText(20),
            'sort' => fake()->numberBetween(0,1000)
        ];
    }
}

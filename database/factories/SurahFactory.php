<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Surah>
 */
class SurahFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'title' => fake('ru_RU')->realText(30),
            'latTitle' => fake('en_US')->realText(30),
            'arabTitle' => fake('ar_SA')->realText(30),
            'description' => fake('ru_RU')->realText(),
            'latDescription' => fake('en_US')->realText(),
            'sort' => fake()->numberBetween(0,1000)
        ];
    }
}

<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Ayat>
 */
class AyatFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'number' => fake()->numberBetween(0,1000),
            'text' => fake('ru_RU')->realText(500),
            'latinText' => fake('en_US')->realText(500),
            'arabText' => fake('ar_SA')->realText(500),
        ];
    }
}

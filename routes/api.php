<?php


use App\Http\Controllers\AuthController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\TagController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [AuthController::class, 'login']);
Route::post('register', [AuthController::class, 'register']);

Route::middleware('auth:sanctum')
    ->group(function () {
        Route::get('auth/me', [AuthController::class, 'me']);
        Route::post('logout', [AuthController::class, 'logout']);


        Route::apiResources([
            'tags' => TagController::class,
            'contacts' => ContactController::class,
        ]);
    });

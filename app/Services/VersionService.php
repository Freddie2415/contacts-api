<?php

namespace App\Services;

use App\Models\Version;
use Str;

class VersionService
{
    static public function update(): void
    {
        $version = Version::first() ?? Version::create(['version' => Str::uuid()]);
        $version?->update(['version' => $version->version = Str::uuid()]);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\User;
use Arr;
use Auth;
use Hash;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Str;

/**
 * @group Auth
 */
class AuthController extends Controller
{
    /**
     * Login
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function login(Request $request): JsonResponse
    {
        $data = $request->validate([
            'email' => 'required',
            'password' => 'required',
            'remember_token' => 'nullable|boolean',
        ]);

        $data['remember_token'] = $data['remember_token'] ?? false;
        $credentials = Arr::except($data, 'remember_token');

        if (Auth::attempt($credentials, $data['remember_token'])) {
            $user = Auth::user();
            $token = Str::afterLast($user->createToken('token')->plainTextToken, '|');

            return response()->json(
                [
                    'user' => $user,
                    'access_token' => $token,
                ]
            );
        }

        return response()->json([
            'message' => __('auth.failed'),
        ], 401);
    }

    /**
     * Logout
     *
     * @authenticated
     * @return JsonResponse
     */
    public function logout(): JsonResponse
    {
        Auth::user()->tokens()->delete();

        return response()->json([], 204);
    }

    /**
     * Register
     *
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function register(Request $request): JsonResponse
    {
        $data = $request->validate([
            'name' => 'required|string',
            'email' => 'required|email|unique:users,email',
            'password' => 'required',
        ]);

        $data['password'] = Hash::make($data['password']);
        $user = User::create($data);

        return response()->json($user);
    }

    /**
     * Get authorized user
     * @authenticated
     * @param Request $request
     * @return JsonResponse
     */
    public function me(Request $request): JsonResponse
    {
        return response()->json($request->user());
    }
}

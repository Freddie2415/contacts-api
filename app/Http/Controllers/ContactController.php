<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreContactRequest;
use App\Http\Requests\UpdateContactRequest;
use App\Models\Contact;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * @authenticated
 * @group Contacts
 */
class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     * @queryParam search The date. Example: name, phone, email
     * @queryParam tag_id The date. Example: 1
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $contacts = Contact::query()
            ->when($request->input('search'), function ($query) use ($request) {
                return $query->where('name', 'ILIKE', '%' . $request->get('search') . '%')
                    ->orWhere('phone', 'ILIKE', '%' . $request->get('search') . '%')
                    ->orWhere('email', 'ILIKE', '%' . $request->get('search') . '%');
            })
            ->when($request->input('tag_id'), function ($query) use ($request) {
                return $query->where('tag_id', '=', $request->get('tag_id'));
            })
            ->get();

        return response()->json($contacts);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreContactRequest $request
     * @return JsonResponse
     */
    public function store(StoreContactRequest $request): JsonResponse
    {
        $contact = Contact::query()->create($request->validated());

        return response()->json($contact, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param Contact $contact
     * @return JsonResponse
     */
    public function show(Contact $contact): JsonResponse
    {
        return response()->json($contact);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateContactRequest $request
     * @param Contact $contact
     * @return JsonResponse
     */
    public function update(UpdateContactRequest $request, Contact $contact): JsonResponse
    {
        $contact->update($request->validated());

        return response()->json($contact);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Contact $contact
     * @return JsonResponse
     */
    public function destroy(Contact $contact): JsonResponse
    {
        $contact->delete();

        return response()->json([], 204);
    }
}

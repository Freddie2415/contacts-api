<?php

namespace App\Observers;

use App\Models\Surah;
use App\Services\VersionService;

class SurahObserver
{
    /**
     * Handle the Surah "created" event.
     *
     * @param  \App\Models\Surah  $surah
     * @return void
     */
    public function created(Surah $surah)
    {
        VersionService::update();
    }

    /**
     * Handle the Surah "updated" event.
     *
     * @param  \App\Models\Surah  $surah
     * @return void
     */
    public function updated(Surah $surah)
    {
        VersionService::update();
    }

    /**
     * Handle the Surah "deleted" event.
     *
     * @param  \App\Models\Surah  $surah
     * @return void
     */
    public function deleted(Surah $surah)
    {
        VersionService::update();
    }

    /**
     * Handle the Surah "restored" event.
     *
     * @param  \App\Models\Surah  $surah
     * @return void
     */
    public function restored(Surah $surah)
    {
        VersionService::update();
    }

    /**
     * Handle the Surah "force deleted" event.
     *
     * @param  \App\Models\Surah  $surah
     * @return void
     */
    public function forceDeleted(Surah $surah)
    {
        VersionService::update();
    }
}

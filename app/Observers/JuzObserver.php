<?php

namespace App\Observers;

use App\Models\Juz;
use App\Services\VersionService;

class JuzObserver
{
    /**
     * Handle the Juz "created" event.
     *
     * @param  \App\Models\Juz  $juz
     * @return void
     */
    public function created(Juz $juz)
    {
        VersionService::update();
    }

    /**
     * Handle the Juz "updated" event.
     *
     * @param  \App\Models\Juz  $juz
     * @return void
     */
    public function updated(Juz $juz)
    {
        VersionService::update();
    }

    /**
     * Handle the Juz "deleted" event.
     *
     * @param  \App\Models\Juz  $juz
     * @return void
     */
    public function deleted(Juz $juz)
    {
        VersionService::update();
    }

    /**
     * Handle the Juz "restored" event.
     *
     * @param  \App\Models\Juz  $juz
     * @return void
     */
    public function restored(Juz $juz)
    {
        VersionService::update();
    }

    /**
     * Handle the Juz "force deleted" event.
     *
     * @param  \App\Models\Juz  $juz
     * @return void
     */
    public function forceDeleted(Juz $juz)
    {
        VersionService::update();
    }
}

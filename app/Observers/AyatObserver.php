<?php

namespace App\Observers;

use App\Models\Ayat;
use App\Models\Version;
use App\Services\VersionService;

class AyatObserver
{
    /**
     * Handle the Ayat "created" event.
     *
     * @param \App\Models\Ayat $ayat
     * @return void
     */
    public function created(Ayat $ayat): void
    {
        VersionService::update();
    }

    /**
     * Handle the Ayat "updated" event.
     *
     * @param \App\Models\Ayat $ayat
     * @return void
     */
    public function updated(Ayat $ayat): void
    {
        VersionService::update();
    }

    /**
     * Handle the Ayat "deleted" event.
     *
     * @param \App\Models\Ayat $ayat
     * @return void
     */
    public function deleted(Ayat $ayat): void
    {
        VersionService::update();
    }

    /**
     * Handle the Ayat "restored" event.
     *
     * @param \App\Models\Ayat $ayat
     * @return void
     */
    public function restored(Ayat $ayat): void
    {
        VersionService::update();
    }

    /**
     * Handle the Ayat "force deleted" event.
     *
     * @param \App\Models\Ayat $ayat
     * @return void
     */
    public function forceDeleted(Ayat $ayat): void
    {
        VersionService::update();
    }
}

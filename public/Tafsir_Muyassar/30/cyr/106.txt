<ayat>
لِإِيلَافِ قُرَيْشٍ ‎﴿١﴾‏ إِيلَافِهِمْ رِحْلَةَ الشِّتَاءِ وَالصَّيْفِ ‎﴿٢﴾
---

1, 2. Қурайшнинг тотув ва хавфсиз яшашини, ишлари бир текис юриб турганини, керакли эҳтиёжларини келтириш учун қишда Яманга, ёзда Шомга қиладиган тижорат сафарлари тартибли равишда йўлга қўйилганини ва бу уларга осон-қулай қилиб қўйилганини кўринг!

</ayat>
<ayat>
فَلْيَعْبُدُوا رَبَّ هَٰذَا الْبَيْتِ ‎﴿٣﴾
---

3. Шундай экан, улар ўзларининг шундай иззат-шарафга эришишларига сабаб бўлган бу Уйнинг – Каъбанинг Роббига шукр ва қуллик қилсинлар. Уни ягона ҳақ илоҳ деб билиб, ёлғиз Унга ибодат қилсинлар.

</ayat>
<ayat>
الَّذِي أَطْعَمَهُم مِّن جُوعٍ وَآمَنَهُم مِّنْ خَوْفٍ ‎﴿٤﴾‏
---

4. У уларни қаттиқ очликдан қутқариб, тўйдирган, кучли қўрқув ва хавф-хатардан омон қилган Зотдир.

</ayat>